
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Retention</title>
        <link href="style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <div class="header">
            <h1>DROM
            <a class ='logo' href="#"><img src='logo.jpg'></a>
            </h1>
        </div>
        <?php
        $array=get_days(strtotime("-1 month 03:00:00"), strtotime("this day 03:00:00"));
        echo '<table border="1">';
        echo '<tr>';
            echo '<td>'.'Time'.'</td>';
            echo '<td>'.'Day1'.'</td>';
            echo '<td>'.'Day7'.'</td>';
            echo '<td>'.'Day30'.'</td>';
        echo '</tr>';
        for ($tr=0; $tr < count($array[0]);$tr++) {
            echo '<tr>';
            echo "<td>" . gmdate("d-m-Y     ", $array[0][$tr][0]) . "</td>";
            for ($period=0;$period<count($array); $period++){
                echo "<td>" . $array[$period][$tr][1] . "</td>";
            }
            echo '</tr>';
        }
        echo'</table>';
?>
    </body>
    <footer>
    <?php
        echo date(' d.m.Y H:i', strtotime ("-1 hours"));
        ?>
    </footer>
</html>


<?
        /**
 * Возвращает данные с сервера
 * @param int $start_ts ...
 * @param int $ens_ts ...
 * @return array ...
 */
function get_days($start_ts, $ens_ts) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://fabric.io/api/v2/organizations/555533a4d91421e517000190/apps/57791460893cfa6537000024/"
        . "growth_analytics/retention.json?start=$start_ts&end=$ens_ts&metric=all&transformation=seasonal");
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5 GTB6");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

    $headers = array();
    $headers[] = "Cookie: G_ENABLED_IDPS=google; _ga=GA1.2.504151272.1530891295; _fabric_session=9CzoZhHJQJYwzVCD7ckPwX8GpRw; notification_key=MPxvt7rfrGga%2FwUGnYupwt3Qf4AJ6Ekw36thKJoM14Q%3D";
    $headers[] = "Accept-Encoding: gzip, deflate, br";
    $headers[] = "X-Csrf-Token: 264bK6eUsWSrkRJR8GwJi3TjqTHBJ9NS/l1h1CNg4BA=";
    $headers[] = "Accept-Language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7";
    $headers[] = "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";
    $headers[] = "Accept: */*";
    $headers[] = "Referer: https://fabric.io/drom/android/apps/ru.drom.myauto.ladapriora/dashboard/retention";
    $headers[] = "X-Requested-With: XMLHttpRequest";
    $headers[] = "Connection: keep-alive";
    $headers[] = "X-Crashlytics-Developer-Token: 0bb5ea45eb53fa71fa5758290be5a7d5bb867e77";
   
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close ($ch);

    $result = json_decode($result);

    $result_arr = array(
        $result->days->day1,
        $result->days->day7,
        $result->days->day30,
    );

    return $result_arr;
}
































